/ chball

	cmp	(sp)+,$2
	bne	1f

	tst	(sp)+
	mov	(sp),r1
	clr	r0
	sys	gtty; ttybuf
	cmpb	(r1),$'e
	beq	2f

	cmpb	(r1),$'c
	beq	3f

1:
	mov	$1,r0
	sys	write; mes; 2
	sys	exit

2:
	bic	$2000,ttybuf+4
	br	1f

3:
	bis	$2000,ttybuf+4

1:
	clr	r0
	sys	stty; ttybuf
	sys	exit

mes:	<?\n>

	.bss
	
ttybuf:	.=.+6
