main() {
	extrn argv;
	extrn even, odd, raw, nl, echo, lcase, tabs, delay;
	auto mode 3;
	auto set, reset, i, p;

	if(argv[0] < 2) {
		write(1, "Arg count*n", 10);
		exit();
	}

	i = 0;
	gtty(1, mode);
	set = mode[2];
loop:
	p = &set;
next:
	switch(char(argv[2], i++)) {
	case 'e':
		*p =| even;
		goto loop;
	case 'o':
		*p =| odd;
		goto loop;
	case 'r':
		*p =| raw;
		goto loop;
	case 'c':
		*p =| nl;
		goto loop;
	case 'f':
		*p =| echo;
		goto loop;
	case 'l':
		*p =| lcase;
		goto loop;
	case 't':
		*p =| tabs;
		goto loop;
	case 'd':
		*p =| delay;
		goto loop;
	case '-':
		p = &reset;
		goto next;
	case 0:
		mode[2] = set & -reset-1;
		stty(1, mode);
		exit();
	}
	write(1, "Bad option*n", 11);
}

even	0200;
odd	0100;
raw	040;
nl	020;
echo	010;
lcase	04;
tabs	02;
delay	01;
