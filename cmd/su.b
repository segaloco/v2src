main() {
	extrn argv;
	auto p, i;

	if(argv[0] != 2)
		goto error;
	p = argv[2];
	i = 0;
	while(i < 5)
		if(char(p, i) != char("", i++))
			goto error;
	if(setuid(0) < 0)
		goto error;
	execl("/bin/sh", "-", 0);
error:
	write(1, "Sorry*n", 6);
}
