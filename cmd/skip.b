main() {
	extrn argv;
	auto i, n, c;

	if(argv[0] < 2)
		return;

	n = i = 0;
	while((c = char(argv[2], i++)) != 0) {
		c =- '0';
		if(c < 0 | c > 9) {
			write(1, "skip error*n", 11);
			return;
		}
		n = 10*n + c;
	}
	i = 0;
	while(i++ < n)
		while((c = getchar()) != '*n') {
			if(c == 0)
				return;
		}
	return;
}
