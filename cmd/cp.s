/ cp -- copy

	mov	sp,r5
	cmp	(r5)+,$3
	bne	argerr

	tst	(r5)+
	mov	(r5)+,0f
	sys	open; 0:..; 0
	bes	oprderr
	mov	r0,fold

	mov	(r5)+,0f
	sys	creat; 0:..; 17
	bes	opwrerr
	mov	r0,fnew

loop:
	mov	fold,r0
	sys	read; buf; 512.
	bes	readerr
	mov	r0,0f
	beq	done
	mov	fnew,r0
	sys	write; buf; 0:..
	bes	writerr
	cmp	r0,0b
	beq	loop

writerr:
	mov	$writ,r1
	br	prerr

readerr:
	mov	$read,r1
	br	prerr

opwrerr:
	mov	$opwr,r1
	br	prerr

oprderr:
	mov	$oprd,r1
	br	prerr

argerr:
	mov	$argc,r1

prerr:
	mov	r1,0f
	mov	$1,r0
	sys	write; 0:..; 5

done:
	sys	exit

argc:	<argc\n>
oprd:	<oprd\n>
opwr:	<opwr\n>
read:	<read\n>
writ:	<writ\n>
	.even

	.bss

fold: .=.+2
fnew: .=.+2
buf: .=.+512
