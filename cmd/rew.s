/ rew -- rewind dec tape

	cmp	(sp)+,$2
	blt	1f
	tst	(sp)+
	mov	(sp)+,r0
	movb	(r0)+,tapx+8
	tstb	(r0)
	bne	error
1:
	sys	open; tapx; 0
	bes	error
	sys	read; word; 2
	bec	done

error:
	mov	$1,r0
	sys	write; 0f; 2

done:
	sys	exit
0:
	<?\n>

tapx:
	</dev/tap0\0>
	.even

.bss
word:	.=.+2

