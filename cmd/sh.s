/ sh -- command interpreter

	mov	sp,r5
	mov	r5,shellarg
	cmpb	*2(r5),$'-
	bne	2f
	sys	intr; 0
	sys	quit; 0
2:
	sys	getuid
	tst	r0
	bne	2f
	movb	$'#,at
2:
	cmp	(r5),$1
	ble	newline
	clr	r0
	sys	close
	mov	4(r5),0f
	sys	open; 0:..; 0
	bec	1f
	jsr	r5,error
		<Input not found\n\0>; .even
	sys	exit
1:
	clr	at
newline:
	tst	at
	beq	newcom
	mov	$1,r0
	sys	write; at; 2.
newcom:
	mov	shellarg,sp
	mov	$parbuf,r3
	mov	$parp,r4
	clr	infile
	clr	outfile
	clr	glflag
newarg:
	jsr	pc,blank
	jsr	r5,delim
		br 2f
	mov	r3,-(sp)
	cmp	r0,$'<
	bne	1f
	mov	(sp),infile
	clr	(sp)
	br	3f
1:
	cmp	r0,$'>
	bne	newchar
	mov	(sp),outfile
	clr	(sp)
	br	3f
newchar:
	cmp	$' ,r0
	beq	1f
	cmp	$'\n+200,r0
	beq	1f
	jsr	pc,putc
3:
	jsr	pc,getc
	jsr	r5,delim
		br 1f
	br	newchar
1:
	clrb	(r3)+
	mov	(sp)+,(r4)+
	bne	1f
	tst	-(r4)
1:
	jsr	r5,delim
		br 2f
	br	newarg
2:
	clr	(r4)
	mov	r0,-(sp)
	jsr	pc,docom
	cmpb	(sp),$'&
	beq	newcom
	tst	r1
	beq	2f
1:
	sys	wait
	bcs	2f
	cmp	r0,r1
	bne	1b
2:
	cmp	(sp),$'\n
	beq	newline
	br	newcom
docom:
	sub	$parp,r4
	bne	1f
	clr	r1
	rts	pc
1:
	jsr	r5,chcom; qchdir
		br 2f
	cmp	r4,$4
	beq	3f
	jsr	r5,error
		<Arg count\n\0>; .even
	br	4f
3:
	mov	parp+2,0f
	sys	chdir; 0:0
	bec	4f
	jsr	r5,error
		<Bad directory\n\0>; .even
4:
	clr	r1
	rts	pc
2:
	jsr	r5,chcom; glogin
		br 2f
	sys	exec; parbuf; parp
	sys	exec; binpb; parp
2:
	sys	fork
		br newproc
	bec	1f
	jsr	r5,error
		<Try again\n\0>; .even
	jmp	newline
1:
	mov	r0,r1
	rts	pc

error:
	movb	(r5)+,och
	beq	1f
	mov	$1,r0
	sys	write; och; 1
	br	error
1:
	inc	r5
	bic	$1,r5
	clr	r0
	sys	seek; 0; 2
	rts	r5

chcom:
	mov	(r5)+,r1
	mov	$parbuf,r2
1:
	movb	(r1)+,r0
	cmpb	(r2)+,r0
	bne	1f
	tst	r0
	bne	1b
	tst	(r5)+
1:
	rts	r5

putc:
	cmp	r0,$''
	beq	1f
	cmp	r0,$'"
	beq	1f
	bic	$!177,r0
	movb	r0,(r3)+
	rts	pc
1:
	mov	r0,-(sp)
1:
	jsr	pc,getc
	cmp	r0,$'\n
	bne	2f
	jsr	r5,error
		<"' imbalance\n\0>; .even
	jmp	newline
2:
	cmp	r0,(sp)
	beq	1f
	bic	$!177,r0
	movb	r0,(r3)+
	br	1b
1:
	tst	(sp)+
	rts	pc

/ the new process

newproc:
	mov	infile,0f
	beq	1f
	tstb	*0f
	beq	3f
	clr	r0
	sys	close
	sys	open; 0:..; 0
	bcc	1f
3:
	jsr	r5,error
		<Input file\n\0>; .even
	sys	exit
1:
	mov	outfile,r2
	beq	1f
	cmpb	(r2),$'>
	bne	4f
	inc	r2
	mov	r2,0f
	sys	open; 0:..; 1
	bec	3f
4:
	mov	r2,0f
	sys	creat; 0:..; 17
	bec	3f
2:
	jsr	r5,error
		<Output file\n\0>; .even
	sys	exit
3:
	sys	close
	mov	r2,0f
	mov	$1,r0
	sys	close
	sys	open; 0:..; 1
	sys	seek; 0; 2
1:
	tst	glflag
	bne	1f
	sys	exec; parbuf; parp
	sys	exec; binpb; parp
2:
	jsr	r5,error
		<No command\n\0>; .even
	sys	exit
1:
	mov	$glob,parp-2
	sys	exec; glob; parp-2
	br	2b

delim:
	cmp	r0,$'\n
	beq	1f
	cmp	r0,$'&
	beq	1f
	cmp	r0,$';
	beq	1f
	cmp	r0,$'?
	beq	3f
	cmp	r0,$'*
	beq	3f
	cmp	r0,$'[
	bne	2f
3:
	inc	glflag
2:
	tst	(r5)+
1:
	rts	r5

blank:
	jsr	pc,getc
	cmp	$' ,r0
	beq	blank
	cmp	r0,$200+'\n
	beq	blank
	rts	pc
getc:
	tst	param
	bne	2f
	mov	inbufp,r1
	cmp	r1,einbuf
	bne	1f
	jsr	pc,getbuf
	br	getc
1:
	movb	(r1)+,r0
	mov	r1,inbufp
	bis	escap,r0
	clr	escap
	cmp	r0,$'\\
	beq	1f
	cmp	r0,$'$
	beq	3f
	rts	pc
1:
	mov	$200,escap
	br	getc
2:
	movb	*param,r0
	beq	1f
	inc	param
	rts	pc
1:
	clr	param
	br	getc
3:
	jsr	pc,getc
	sub	$'0,r0
	cmp	r0,$9.
	blos	1f
	mov	$9.,r0
1:
	mov	shellarg,r1
	inc	r0
	cmp	r0,(r1)
	bge	getc
	asl	r0
	add	r1,r0
	mov	2(r0),param
	br	getc
getbuf:
	mov	$inbuf,r0
	mov	r0,inbufp
	mov	r0,einbuf
	dec	r0
	mov	r0,0f
1:
	inc	0f
	clr	r0
	sys	read; 0:0; 1
	bcs	xit1
	tst	r0
	beq	xit1
	inc	einbuf
	cmp	0b,$inbuf+256.
	bhis	xit1
	cmpb	*0b,$'\n
	bne	1b
	rts	pc

xit1:
	sys	exit

quest:
	<?\n>

at:
	<@ >

qchdir:
	<chdir\0>
glogin:
	<login\0>
glob:
	</etc/glob\0>
binpb:
	</bin/>
parbuf: .=.+1000.
	.even
param:	.=.+2
glflag:	.=.+2
infile: .=.+2 
outfile:.=.+2
	.=.+2 / room for glob
parp:	.=.+200.
inbuf:	.=.+256.
escap:	.=.+2
inbufp: .=.+2
einbuf:	.=.+2
och:	.=.+2
shellarg:.=.+2