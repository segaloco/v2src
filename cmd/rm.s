/ rm -- remove (unlink) files

rm:
	sys	break; 206
	mov	sp,r5
	mov	(r5),r4
	cmp	(r5)+,(r5)+

3:
	mov	(r5)+,0f
	dec	r4
	bgt	1f
	sys	exit

1:
	mov	0f,2f
	sys	stat; 2:..; stbuf
	bes	err
	bit	$40000,stbuf+2
	bne	err
	sys	unlink; 0:..
	bec	3b

err:
	clr	1f
	mov	0b,r3
	mov	r3,0f

2:
	inc	1f
	tstb	(r3)+
	bne	2b
	mov	$1,r0
	sys	write; 0:..; 1:..
	mov	$1,r0
	sys	write; quest; 3
	br	3b

quest:
	< ?\n>
	.even
.bss
stbuf:	.=.+34.
