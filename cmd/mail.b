main() {
	extrn argv, buf;
	auto ac, mail, f, n, j, i, header 20, k;

	if((ac=argv[0]) < 3)
		goto printm;
	if((mail=open(argv[2],0)) < 0) {
		printf("Cannot open `%s'*n", argv[2]);
		exit();
	}
	if((f=open("/etc/uids",0)) < 0)
		goto noname;

	n = read(f,buf,512);
	close(f);

	j = 0;
	k = getuid();
	while(j < n) {
		f = j;
		while(char(buf, j++) != ':');
		lchar(buf, j-1, 0);
		i = 0;
		while(char(buf, j) != '*n')
			i = i*10 + char(buf, j++) - '0';
		j++;
		if(i == k)
			goto namefound;
	}
noname:
	printf("Who are you?*n");
	exit();

namefound:
	k = append(header, 0, "From ");
	if(f & 1) {
		lchar(buf, --f, ' ');
		k--;
	}
	k = append(header, k, &buf[f/2]);
	time(buf);
	ctime(buf, buf);
	k = append(header, k, " ");
	k = append(header, k, buf);
	k = append(header, k, "*n");

send:
	i = append(buf, 0, "/usr/");
	j = 0;
	while((n=char(argv[ac], j++)) != 0) {
		if(n == '/')
			i = 0;
	}
	i = append(buf, i, argv[ac]);
	append(buf, i, "/mailbox");

	if((f=open(buf,1)) < 0)
		if((f=creat(buf,017)) < 0) {
			printf("Cannot send to `%s'*n", buf);
			goto next;
		}
	seek(mail,0,0);
	seek(f,0,2);
	write(f, header, k);
	while((n=read(mail,buf,512)) > 0)
		write(f,buf,n);
	write(f,"*n*n",2);
	close(f);

next:
	if(--ac > 2)
		goto send;
	exit();

printm:
	if((mail=open("mailbox", 0)) < 0) {
		printf("No mail*n");
		exit();
	}
	unlink("mail");
	link("mailbox", "mail");
	unlink("mailbox");
	while((n = read(mail, buf, 512)) > 0)
		write(1, buf, n);
	if(ac == 2) {
		buf = argv[2];
		goto save;
	}
	printf("Save? ");
	read(0, buf, 512);
save:
	if(char(buf, 0) != 'y')
		unlink("mail");
	else
		printf("Saved in `mail'*n");
}

append(s1, p1, s2) {
	auto i, c;

	i = 0;
	while((c=char(s2, i++)) != 0)
		lchar(s1, p1++, c);
	lchar(s1, p1, 0);
	return(p1);
}

buf[256];
