/ tty -- get tty number

	clr	r0
	sys	fstat; stbuf
	mov	stbuf,r0
	sub	$14.,r0
	cmp	r0,$16.
	bcc	err
	add	$'0,r0
	movb	r0,nam
	mov	$1,r0
	sys	write; name; 5
	sys	exit

err:
	mov	$1,r0
	sys	write; errmsg; 13
	sys	exit

name:
	<tty>
nam:
	<\0\n>
errmsg:
	<Not a tty\n\0>
.even

	.bss

stbuf:	.=.+34.
