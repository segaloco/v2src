main() {
	extrn argv;
	auto i, oav, c, ac, ap, ino;
	auto ab 1000, ava 150;
	auto dirf, string, av, name 5, s;

	if ((ac=argv[0]) < 3) {
		write(1, "Arg count*n", 10);
		exit();
	}

	av = 0;
	string = ab;
	ap = 2;
loop:
	i = 0;
	s = argv[ap++];
	while(c=char(s, i++)) {
		if(c=='**' | c=='?' | c=='[')
			goto expand;
	}
	ava[av++] = s;
	if(--ac >= 2)
		goto loop;
	goto execute;
expand:
	if (char(s, --i) == '/') {
		lchar(s, i, 0);
		if((dirf = open(i==0? "/": s, 0)) < 0)
			goto err;
		lchar(s, i++, '/');
		goto dir;
	}
	if (i>0)
		goto expand;
	if((dirf = open(".", 0)) >= 0)
		goto dir;
err:
	write(1, "No directory*n", 13);
	exit();

dir:
	oav = av;
dirloop:
	while (read(dirf, &ino, 2) > 0) {
		read(dirf, name, 8);
		if (ino==0)
			goto dirloop;
		if (match(name, s, i)) {
			ava[av++] = string;
			cat(string, 0, s);
			string =+ (1+cat(string, i, name))/2;
		}
	}
	close(dirf);

	i = oav;
	while (i < av-1) {
		oav = i;
		while (++oav < av) {
			if (compar(ava[i], ava[oav])) {
				c = ava[i];
				ava[i] = ava[oav];
				ava[oav] = c;
			}
		}
		i++;
	}

	if(--ac >= 2)
		goto loop;

execute:
	if(av <= 1) {
		write(1, "No match*n", 9);
		exit();
	}
	execv(ava[0], ava, av);
	i = cat(string, 0, "/bin/");
	cat(string, --i, ava[0]);
	execv(string, ava, av);
	write(1, "No command*n", 11);
}

match(s, p, q) {
	if (char(s, 0)=='.')
		return(0);
	return(amatch(s, 0, p, q));
}

amatch(s1, p1, s2, p2) {
	auto c, cc, ok, lc, scc;

	scc = char(s1, p1);
	lc = 077777;
	switch(c = char(s2, p2)) {

	case '[':
		ok = 0;
		while (cc = char(s2, ++p2)) {
			switch(cc) {

			case ']':
				if (ok)
					return(amatch(s1, ++p1, s2, ++p2));
				else
					return(0);

			case '-':
				ok =+ lc<=scc & scc<=(cc=char(s2, p2+1));
			}
			if (scc == (lc=cc))
				ok++;
		}
		return(0);

	case '?':
	quest:
		if (scc)
			return(amatch(s1, ++p1, s2, ++p2));
		return(0);

	case '**':
		return(umatch(s1, p1, s2, ++p2));

	case '*0':
		return(!scc);
	}
	if (c==scc)
		goto quest;
	return(0);
}

umatch(s1, p1, s2, p2) {
	if(char(s2, p2)==0)
		return(1);
	while(char(s1, p1))
		if(amatch(s1, p1++, s2, p2))
			return(1);
	return(0);
}

compar(s1, s2) {
	auto c1, c2, p1, p2;

	p1 = p2 = 0;
loop:
	if((c1=char(s1, p1++))==0)
		return(0);
	if((c2=char(s2, p2++))==0)
		return(1);
	if(c1==c2)
		goto loop;
	return(c1>c2);
}

cat(s1, p1, s2) {
	auto c, i;

	i = 0;
	while(c=char(s2, i++))
		lchar(s1, p1++, c);
	lchar(s1, p1++, 0);
	return(p1);
}
