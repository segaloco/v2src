main() {
	extrn argv;
	auto line 32;

	if (!gtty(0, line) | argv[0]<=1) {
		write(1, "goto error*n", 11);
		seek(0, 0, 2);
		return;
	}
	seek(0, 0, 0);

loop:
	if (getlin(line)) {
		write(1, "label not found*n", 16);
		return;
		}
	if (compar(line, argv[2])) goto loop;
}

getlin(s) {
	auto i, ch;

	i = 0;
l:
	if ((ch=getchar())=='*0') return(1);
	if (ch!=':') {
		while(ch!='*n' & ch!='*0')
			ch = getchar();
		goto l;
		}
	while ((ch=getchar())==' ');
	while (ch!=' ' & ch!='*n' & ch!='*0') {
		lchar(s, i++, ch);
		ch = getchar();
	}
	lchar(s, i++, '*0');
	return(0);
}

compar(s1, s2) {
	auto i, c;

	i = 0;
l:
	if((c=char(s1, i)) != char(s2, i++)) return(1);
	if (c=='*0') return(0);
	goto l;
}
