main() {
	extrn argv, ac, ap;
	auto np, na, ncom, nargv 50, c, i;

	ac = argv[0];
	if (ac<2) return;
	ap = 2;
	if (exp()) {
		np = 0;
		while (na=nxtarg())
			nargv[np++] = na;
		nargv[np] = 0;
		if (np==0) return;
		execv(nargv[0], nargv, np);
		i = 0;
		ncom = "/bin/xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx";
		while(c=char(nargv[0], i)) {
			lchar(ncom, 5+i++, c);
		}
		lchar(ncom, 5+i, '*0');
		execv(ncom, nargv, np);
		write(1, "no command*n", 11);
		seek(0, 0, 2);
	}
}

nxtarg() {
	extrn argv, ac, ap;

	if (ap>ac) return(0*ap++);
	return(argv[ap++]);
}
ap;
ac;

exp(s) {
	extrn ap;
	auto p1;

	p1 = e1();
	if (nxtarg()[0] == '-o') return (p1 | exp());
	ap--;
	return(p1);
}

e1() {
	extrn ap;
	auto p1;

	p1 = e2();
	if (nxtarg()[0] == '-a') return (p1 & e1());
	ap--;
	return(p1);
}

e2() {
	extrn ap;

	if (nxtarg()[0] == '!')
		return(!e3());
	ap--;
	return(e3());
}

e3() {
	auto p1, a;

	if ((a=nxtarg())==0) goto err;
	switch(a[0]) {
err:
		write(1, "if error*n", 9);
		exit();
	case '(':
		p1 = exp();
		if(nxtarg()[0] != ')') goto err;
		return(p1);
	case '-r':
		return(tio(nxtarg(), 0));
	case '-w':
		return(tio(nxtarg(), 1));
	case '-c':
		return(tcreat(nxtarg()));
	}

	p1 = nxtarg();
	if (p1==0) goto err;
	switch(p1[0]) {
	case '=':
		return(eq(a, nxtarg()));
	case '!=':
		return(!eq(a, nxtarg()));
	}
	goto err;
}

tio(a, f) {
	auto xxx;

	a = open(a, f);
	if(a >= 0) {
		close(a);
		return(1);
	}
	return(0);
}

tcreat(a)
	return(1);

eq(a, b) {
	auto c, i;

	i = 0;
	while((c=char(a, i)) == char(b, i++))
		if(c == '*0')
			return(1);
	return(0);
}
