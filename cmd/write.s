/ write -- write to another user

	cmp	(sp)+,$2
	bge	1f
	mov	$1,r0
	sys	write; argm; eargm-argm
	sys	exit
1:
	tst	(sp)+
	mov	(sp)+,r5
	sys	open; utmp; 0
	bec	1f
	mov	$1,r0
	sys	write; film; efilm-film
	sys	exit
1:
	mov	r0,ufil
1:
	mov	ufil,r0
	sys	read; ubuf; 16.
	tst	r0
	beq	8f
	mov	$ubuf,r3
	mov	r5,r4
	mov	$9.,r2
2:
	dec	r2
	beq	2f
	cmpb	(r4)+,(r3)+
	beq	2b
	tstb	-1(r4)
	bne	1b
	cmpb	$' ,-1(r3)
	bne	1b
2:
	movb	8.+ubuf,ttyno
	sys	open; ttyx; 1
	bec	2f
	mov	$1,r0
	sys	write; dnymes; ednymes-dnymes
	sys	exit
2:
	mov	r0,ttyf
	sys	stat; tty0; statbuf
	bes	unknown
	mov	statbuf,r4
	clr	r0
	sys	fstat; statbuf
	mov	statbuf,r3
	sub	r4,r3
	add	$60,r3
	mov	ufil,r0
	sys	seek; 0; 0
1:
	mov	ufil,r0
	sys	read; ubuf; 16.
	tst	r0
	beq	unknown
	cmp	r3,ubuf+8.
	bne	1b
	mov	$ubuf,r0
	mov	$8.,r1
2:
	cmpb	$' ,(r0)+
	bne	1f
	clrb	-1(r0)
1:
	dec	r1
	bne	2b
6:
	mov	ttyf,r0
	sys	write; mesg; emesg-mesg
	mov	ttyf,r0
	sys	write; ubuf; 0:10
	mov	ttyf,r0
	sys	write; qnl; 4
	sys	intr; 9f
7:
	clr	r0
	sys	read; ch; 1
	tst	r0
	beq	9f
	tst	nlflg
	beq	1f
	cmp	ch,$'!
	bne	1f
	sys	fork
		br mshproc
	sys	wait
	mov	$1,r0
	sys	write; excl; 2
	br	7b
1:
	clr	nlflg
	cmp	ch,$'\n
	bne	1f
	inc	nlflg
1:
	mov	ttyf,r0
	sys	write; ch; 1
	br	7b
8:
	movb	(r5)+,ch
	beq	8f
	mov	$1,r0
	sys	write; ch; 1
	br	8b
8:
	mov	$1,r0
	sys	write; errmsg; eerrmsg-errmsg
	sys	exit
9:
	mov	ttyf,r0
	sys	write; endmsg; eendmsg-endmsg
	sys	exit

unknown:
	mov	$"??,ubuf
	clr	ubuf+2
	clr	ubuf+4
	clr	ubuf+6
	br	6b

mshproc:
	sys	exec; msh; mshp
	sys	exit

nlflg:
	1

mshp:
	msh
	0
msh:
	</etc/msh\0>
argm:
	<Arg count\n>
eargm:
film:
	<Cannot open utmp\n>
efilm:
excl:
	<!\n>
qnl:
	<...\n>
ttyx:
	</dev/ttyx\0>
ttyno	= .-2
utmp:
	</tmp/utmp\0>
tty0:
	</dev/tty0\0>
endmsg:
	<EOT\n>
eendmsg:
errmsg:
	< not logged in.\n>
eerrmsg:
mesg:
	<\nMessage from >
emesg:
dnymes:
	<Permission denied.\n>
ednymes:
	.even
	.bss

ttyf:	.=.+2
ubuf:	.=.+16.
statbuf:.=.+40.
ch:	.=.+2
ufil:	.=.+2
signal = 48.
