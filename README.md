# Rationale

This repository represents a disassembly and cross-referencing
effort intending to produce a reasonably accurate source code
restoration of a set of UNIX binaries representing the codebase
at some point between the first and second research editions.
The author(s) make no claims to the accuracy of any components
and disclaim any liabilities related to the use of this code,
up to and including responsibility for ensuring fitness for a
particular purpose and merchantability.
All copyrights, explicit or implicit, are retained by their
respective owners.
See LICENSE for more information.

# Process

This process has made use of the pdp11dasm PDP-11 disassembler[1]
to produce machine readable listings of the various binaries
available in the archive s2-bits.tar.gz[2].

To achieve parity with the known UNIX PDP-11 assembly sources,
the following replacements have been made in the generated text:

```
r6	-> sp
#	-> $
trap	-> sys
bcs	-> bes (when used after a system call)
bcc	-> bec (")
177300	-> div
177302	-> ac
177304	-> mq
177306	-> mul
```

The latter four are particular to PDP-11/20 code when using the
KE-11 extended arithmetic element (eae).
These and other special register mappings can be found in file
a19.s of any version of Dennis Ritchie's UNIX PDP-11 assembler
in the "eae & switches" section of the data segment.

Additionally, pdp11dasm decodes all numerical values in octal so,
where appropriate, many values have been converted to hexadecimal,
decimal, or character representation, depending on the context of
those values' uses.
Finally, system call numbers have been derived from the second
edition of the UNIX Programmer's Manual and their identities used
to reduce binary data following said syscalls into arguments.

Where possible, diffs with known source code from the first, second,
and fifth editions have been used to reconstruct files as well as
determine deltas between the Bell code and the results of the
disassembler.

The applications themselves are a mishmash of naked binaries and
first and second edition a.out formats.
In other words, some binaries immediately begin processing arguments,
some have a "br +14" first edition magic number and a.out header,
while others have a "br +20" second edition magic number and a.out
header.
These headers, if present, are not being included in individual
disassemblies but the "edition" of binaries will be noted in a
later pass when identifying their likely age.

# B Applications

Credit goes to Angelo Papenhoff for the decompiled B applications in this repository.  Further information can be found in [3].

[1] - https://github.com/caldwell/pdp11dasm
[2] - https://www.tuhs.org/Archive/Distributions/Research/1972_stuff/s2-bits.tar.gz
[3] - https://www.tuhs.org/pipermail/tuhs/2023-June/028494.html

